package tdd.training.mra;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

public class MarsRover {
	
	private static final char EAST = 'E';
	private static final char WEST = 'W';
	private static final char NORTH = 'N';
	private static final char SOUTH = 'S';
	
	private int planetX;
	private int planetY;
	private List<String> planetObstacles;
	private int roverX;
	private int roverY;
	private char roverDirection;

	/**
	 * It initializes the rover at the coordinates (0,0), facing North, on a planet
	 * (represented as a grid with x and y coordinates) containing obstacles.
	 * 
	 * @param planetX         The x dimension of the planet.
	 * @param planetY         The y dimension of the planet.
	 * @param planetObstacles The obstacles on the planet. Each obstacle is a string
	 *                        (without white spaces) formatted as follows:
	 *                        "(oi_x,oi_y)". <code>null</code> if the planet does
	 *                        not contain obstacles.
	 * 
	 * @throws MarsRoverException
	 */
	public MarsRover(int planetX, int planetY, List<String> planetObstacles) throws MarsRoverException {
		this.planetX = planetX - 1;
		this.planetY = planetY - 1;
		this.planetObstacles = planetObstacles;
		this.roverX = 0;
		this.roverY = 0;
		this.roverDirection = MarsRover.NORTH;
	}

	/**
	 * It returns whether, or not, the planet (where the rover moves) contains an
	 * obstacle in a cell.
	 * 
	 * @param x The x coordinate of the cell
	 * @param y The y coordinate of the cell
	 * @return <true> if the cell contains an obstacle, <false> otherwise.
	 * @throws MarsRoverException
	 */
	public boolean planetContainsObstacleAt(int x, int y) throws MarsRoverException {
		
		if(x < 0 || x > planetX) throw new MarsRoverException("error x coordinate");
		if(y < 0 || y > planetY) throw new MarsRoverException("error y coordinate");
		
		boolean obstacleIsFound = false;
		
		for (String obstacle : planetObstacles) {
			
			String[] parts = obstacle.split(",");
			
			int xObstacle = Integer.parseInt(parts[0].replaceAll("[^0-9]", ""));
			int yObstacle = Integer.parseInt(parts[1].replaceAll("[^0-9]", ""));
			
			if(xObstacle == x && yObstacle == y) obstacleIsFound = true;
			
		}
		
		return obstacleIsFound;
	}

	/**
	 * It lets the rover move on the planet according to a command string. The
	 * return string contains the new position of the rover, its direction, and the
	 * obstacles it has encountered while moving on the planet (if any).
	 * 
	 * @param commandString A string that can contain a single command -- i.e. "f"
	 *                      (forward), "b" (backward), "l" (left), or "r" (right) --
	 *                      or a combination of single commands.
	 * @return The return string that contains the position and direction of the
	 *         rover, and the obstacles the rover has encountered while moving on
	 *         the planet (if any). The return string (without white spaces) has the
	 *         following format: "(x,y,dir)(o1_x,o1_y)(o2_x,o2_y)...(on_x,on_y)". x
	 *         and y define the new position of the rover while dir represents its
	 *         direction (i.e., N, S, W, or E). Finally, oi_x and oi_y are the
	 *         coordinates of the i-th encountered obstacle.
	 * @throws MarsRoverException
	 */
	public String executeCommand(String commandString) throws MarsRoverException {
		
		Set<String> encounteredObstacles = new LinkedHashSet<>();
		
		for (char command : commandString.toCharArray()) {
			
			if(command == 'r') this.turnRight();
			
			if(command == 'l') this.turnLeft();
			
			if(command == 'f') this.goForward();
			
			if(command == 'b') this.goBackward();
			
			if(planetContainsObstacleAt(roverX, roverY)) {
				
				encounteredObstacles.add("(" + roverX + "," + roverY + ")");
				
				if(command == 'f') this.goBackward();
				if(command == 'b') this.goForward();
				
			}
			
		}
		
		String encounteredObstaclesString = "";
		
		for(String obstacle : encounteredObstacles) {
			
			encounteredObstaclesString += obstacle;
			
		}
		
		return roverToString() + encounteredObstaclesString;
	}
	
	private String roverToString() {
		
		return "(" + roverX + "," + roverY + "," + this.roverDirection + ")";
		
	}
	
	private void turnRight() {
		
		if(roverDirection == MarsRover.NORTH) {
			
			roverDirection = MarsRover.EAST;
			return;
			
		}
		
		if(roverDirection == MarsRover.EAST) {
			
			roverDirection = MarsRover.SOUTH;
			return;
			
		}
		
		if(roverDirection == MarsRover.SOUTH) {
			
			roverDirection = MarsRover.WEST;
			return;
			
		}
		
		if(roverDirection == MarsRover.WEST) {
			
			roverDirection = MarsRover.NORTH;
			return;
		}
		
	}
	
	private void turnLeft() {
		
		if(roverDirection == MarsRover.NORTH) {
			
			roverDirection = MarsRover.WEST;
			return; 
			
		}
		
		if(roverDirection == MarsRover.EAST) {
			
			roverDirection = MarsRover.NORTH;
			return; 
			
		}
		
		if(roverDirection == MarsRover.SOUTH) {
			
			roverDirection = MarsRover.EAST;
			return; 
			
		}
		
		if(roverDirection == MarsRover.WEST) {
			
			roverDirection = MarsRover.SOUTH;
			return; 
			
		}
		
	}
	
	private void goForward() throws MarsRoverException {
		
		if(roverDirection == MarsRover.NORTH) {
			if(roverY + 1 > planetY) roverY = 0;
			else roverY++;
		}
		
		if(roverDirection == MarsRover.EAST) {
			if(roverX + 1 > planetX) roverX = 0;
			else roverX++;
		}
		
		if(roverDirection == MarsRover.SOUTH) {
			if(roverY - 1 < 0) roverY = planetY;
			else roverY--;
		}
		
		if(roverDirection == MarsRover.WEST) {
			if(roverX - 1 < 0) roverX = planetX;
			else roverX --;
		}
		
	}
	
	private void goBackward() throws MarsRoverException {
		
		if(roverDirection == MarsRover.NORTH) {
			if(roverY - 1 < 0) roverY = planetY;
			else roverY--;
		}
		
		if(roverDirection == MarsRover.EAST) {
			if(roverX - 1 < 0) roverX = planetX;
			else roverX--;
		}
		
		if(roverDirection == MarsRover.SOUTH) {
			if(roverY + 1 > planetY) roverY = 0;
			else roverY++;
		}
		
		if(roverDirection == MarsRover.WEST) {
			if(roverX + 1 > planetX) roverX = 0;
			else roverX++;
		}
		
	}

}
