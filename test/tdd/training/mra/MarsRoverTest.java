package tdd.training.mra;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class MarsRoverTest {

	@Test
	public void testCorrecthObstacle() throws Exception {
		
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(5,5)");
		planetObstacles.add("(7,8)");
		
		MarsRover marsRover = new MarsRover(10, 10, planetObstacles);
		boolean obstacle = marsRover.planetContainsObstacleAt(7, 8);
		
		assertTrue(obstacle);
	}
	
	@Test
	public void testIncorrectObstacle() throws Exception {
		
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(5,5)");
		planetObstacles.add("(7,8)");
		
		MarsRover marsRover = new MarsRover(10, 10, planetObstacles);
		boolean obstacle = marsRover.planetContainsObstacleAt(8, 8);
		
		assertFalse(obstacle);
	}

	@Test (expected = MarsRoverException.class)
	public void testErrorObstacle() throws Exception {
		
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(5,5)");
		planetObstacles.add("(7,8)");
		
		MarsRover marsRover = new MarsRover(10, 10, planetObstacles);
		marsRover.planetContainsObstacleAt(9, 10);
	}
	
	@Test
	public void testEmptyCommandString() throws Exception {
		
		List<String> planetObstacles = new ArrayList<>();
		MarsRover marsRover = new MarsRover(10, 10, planetObstacles);
			
		assertEquals(marsRover.executeCommand(""), "(0,0,N)");
	}
	
	@Test
	public void testTurnRight() throws Exception {
		
		List<String> planetObstacles = new ArrayList<>();
		MarsRover marsRover = new MarsRover(10, 10, planetObstacles);
			
		assertEquals(marsRover.executeCommand("r"), "(0,0,E)");
	}
	
	@Test
	public void testTurnRightTwice() throws Exception {
		
		List<String> planetObstacles = new ArrayList<>();
		MarsRover marsRover = new MarsRover(10, 10, planetObstacles);
		marsRover.executeCommand("r");
			
		assertEquals(marsRover.executeCommand("r"), "(0,0,S)");
	}
	
	@Test
	public void testTurnLeft() throws Exception {
		
		List<String> planetObstacles = new ArrayList<>();
		MarsRover marsRover = new MarsRover(10, 10, planetObstacles);
			
		assertEquals(marsRover.executeCommand("l"), "(0,0,W)");
	}
	
	@Test
	public void testMoveForwardNorth() throws Exception {
		
		List<String> planetObstacles = new ArrayList<>();
		MarsRover marsRover = new MarsRover(10, 10, planetObstacles);
			
		assertEquals(marsRover.executeCommand("f"), "(0,1,N)");
	}
	
	@Test
	public void testMoveForwardRight() throws Exception {
		
		List<String> planetObstacles = new ArrayList<>();
		MarsRover marsRover = new MarsRover(10, 10, planetObstacles);
		
		marsRover.executeCommand("r");
			
		assertEquals(marsRover.executeCommand("f"), "(1,0,E)");
	}
	
	@Test
	public void testMoveBackwardNorth() throws Exception {
		
		List<String> planetObstacles = new ArrayList<>();
		MarsRover marsRover = new MarsRover(10, 10, planetObstacles);
		
		marsRover.executeCommand("f");
			
		assertEquals(marsRover.executeCommand("b"), "(0,0,N)");
	}
	
	@Test
	public void testExecuteSequeceOfSingleCommands() throws Exception {
		
		List<String> planetObstacles = new ArrayList<>();
		MarsRover marsRover = new MarsRover(10, 10, planetObstacles);
			
		assertEquals(marsRover.executeCommand("ffrff"), "(2,2,E)");
	}
	
	@Test
	public void testExecuteWrappingCommand() throws Exception {
		
		List<String> planetObstacles = new ArrayList<>();
		MarsRover marsRover = new MarsRover(10, 10, planetObstacles);
			
		assertEquals(marsRover.executeCommand("b"), "(0,9,N)");
	}
	
	@Test
	public void testExecuteEncounteringSingleObstacle() throws Exception {
		
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(2,2)");
		MarsRover marsRover = new MarsRover(10, 10, planetObstacles);
			
		assertEquals(marsRover.executeCommand("ffrfff"), "(1,2,E)(2,2)");
	}
	
	@Test
	public void testExecuteEncounteringMultipleObstacles() throws Exception {
		
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(2,2)");
		planetObstacles.add("(2,1)");
		MarsRover marsRover = new MarsRover(10, 10, planetObstacles);
			
		assertEquals(marsRover.executeCommand("ffrfffrflf"), "(1,1,E)(2,2)(2,1)");
	}
	
	@Test
	public void testExecuteEncounteringObstacleBeyondAnEdge() throws Exception {
		
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(0,9)");
		MarsRover marsRover = new MarsRover(10, 10, planetObstacles);
			
		assertEquals(marsRover.executeCommand("b"), "(0,0,N)(0,9)");
	}
	
	@Test
	public void testExecuteTourAroundThePlanet() throws Exception {
		
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(2,2)");
		planetObstacles.add("(0,5)");
		planetObstacles.add("(5,0)");
		MarsRover marsRover = new MarsRover(6, 6, planetObstacles);
			
		assertEquals(marsRover.executeCommand("ffrfffrbbblllfrfrbbl"), "(0,0,N)(2,2)(0,5)(5,0)");
	}
}
